interface Subscription {
  "@type": "Subscription";
  id: number
  email: string;
  purpose: string;
  createdAt: Date;
}
