interface Invitation {
  "@type": "Invitation";
  id: number
  sender: string;
  emails: string[];
  firstname: string;
  lastname: string;
  createdAt: Date;
}
