# build
FROM node:13

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=80

COPY ./ /var/etikbnb-web

WORKDIR /var/etikbnb-web

RUN yarn install
RUN yarn build

ENTRYPOINT /var/etikbnb-web/entrypoint.sh
