#!/bin/bash -eux

if [[ $# -eq 0 ]] ; then
    echo 'usage: ./deploy.sh vX.Y.Z'
    exit 0
fi

kubens etikbnb-org
git pull
cat ./k8s-prod.yml | sed "s/latest/$1/" | kubectl apply -f -