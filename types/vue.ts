import { Store } from 'vuex'

import { EnvStore } from '~/stores/EnvStore';
import { Api } from '~/plugins/api.client'

declare module 'vue/types/vue' {
    export interface Vue {
        $api: Api
    }
}

/**
 * inject module store in root store. cf: store/index.ts
 */
declare module 'vuex/types/index' {
    export interface Store<S> {
        $envStore: EnvStore
    }
}

/**
 * inject root store in VuexModule. cf: store/index.ts
 */
declare module 'vuex-module-decorators/dist/types/vuexmodule' {
    export interface VuexModule {
        store: Store<any>
    }
}
