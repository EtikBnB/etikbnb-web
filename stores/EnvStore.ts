import {Module, VuexModule, MutationAction} from 'vuex-module-decorators'

export const DEFAULT_API_URL = "http://localhost:3000";

@Module({
    name: 'envStore',
    stateFactory: true,
    namespaced: true
})
export class EnvStore extends VuexModule {
    config: {[key:string] : string} = { API_URL : "http://localhost:3000" };

    @MutationAction({rawError: true, mutate: ['config']})
    async init(config: {[key:string] : string})  {
      return {
        config : Object.assign(config, this.config)
      }
    }
}
