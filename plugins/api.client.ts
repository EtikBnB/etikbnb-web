import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, Method } from 'axios';
import { Context } from '@nuxt/types'

const DEFAULT_CONF: AxiosRequestConfig = {
    headers: { 'Content-Type': 'application/ld+json' }
};

export const TRANSLATE_API_ERROR: Map<string, string> = new Map([
  ["preinscription.email.alreadyExist", "Cet email est déjà enregistré."],
  ["preinscription.email.invalid", "Le format de l'email est invalide."],
  ["preinscription.purpose.invalid", "Le choix ne fait pas partie des propositions valide."],
  ["invitation.email.alreadyShared", "L'envoie des invitations pour votre mail ont déjà étaient réalisées."]
]);

export class Api {
    public axios: AxiosInstance;

    /**
     * Default constructor.
     * @param baseURL Your api host.
     * @param requestConfig Extend Axios configuration.
     */
    constructor(
        baseURL: string,
        requestConfig?: AxiosRequestConfig
    ) {
        this.axios = axios.create({
            baseURL,
            ...requestConfig
        });
    }

    public async request<T>(
        method: Method,
        url: string,
        requestConfig?: AxiosRequestConfig
    ): Promise<T> {
        try {
            const response: AxiosResponse<T> = await this.axios.request({
                method,
                responseType: "json",
                url,
                ...requestConfig
            });
            return response.data;
        } catch (e) {
            if (e?.response?.data["hydra:description"]) {
                let violations = e?.response?.data["violations"] || [];
                if (violations.length == 0) {
                  violations = [e?.response?.data["hydra:description"] as string]
                } else {
                  violations = violations.map((v: {propertyPath: string, message: string}) => v.message)
                }
                let messages = violations.map((v: string) => TRANSLATE_API_ERROR.has(v) ? TRANSLATE_API_ERROR.get(v) : null).filter((m: string) => m !== null);
                if (messages.length > 0) {
                  throw messages
                }
            }
            throw "<span>Une erreur serveur c'est produite. Merci de rapportez ce bug <a href='https://airtable.com/shr67RsC0SoAkhkO4'>ici</a></span>";
        }
    }

    /**
     * Get a specific entry
     * @param contentTypePluralized Type of entry pluralized
     * @param id ID of entry
     */
    public get<T>(contentTypePluralized: string, id: string): Promise<T> {
        return this.request('get', `/${contentTypePluralized}/${id}`);
    }

    /**
     * Create data
     * @param contentTypePluralized Type of entry pluralized
     * @param data New entry
     */
    public post<T>(
        contentTypePluralized: string,
        data: AxiosRequestConfig['data']
    ): Promise<T> {
        return this.request('post', `/${contentTypePluralized}`, {
            data
        });
    }

    /**
     * Update data
     * @param contentTypePluralized Type of entry pluralized
     * @param id ID of entry
     * @param data
     */
    public put<T>(
        contentTypePluralized: string,
        id: string,
        data: AxiosRequestConfig['data']
    ): Promise<T> {
        return this.request('put', `/${contentTypePluralized}/${id}`, {
            data
        });
    }

    /**
     * Delete an entry
     * @param contentTypePluralized Type of entry pluralized
     * @param id ID of entry
     */
    public delete(
        contentTypePluralized: string,
        id: string
    ): Promise<void> {
        return this.request('delete', `/${contentTypePluralized}/${id}`);
    }

}

export default (context: Context, inject: (key: string, value: any) => void) => {
    if(!context.store.$envStore.config["MOCK"]){
        inject('api', new Api(context.store.$envStore.config.API_URL, DEFAULT_CONF));
    }
}
