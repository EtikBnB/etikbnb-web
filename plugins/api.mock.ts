import { Context } from '@nuxt/types'
import MockAdapter from 'axios-mock-adapter';
import { AxiosRequestConfig } from 'axios';

import { Api, TRANSLATE_API_ERROR } from './api.client'

function configMock(mock: MockAdapter) {

    /**
     * post api/subscriptions:
     *  ko => if email in TRANSLATE_API_ERROR, example: preinscription.email.alreadyExist@ko.com
     *  ok =>
     */
    mock.onPost('/api/subscriptions').reply(async (req: AxiosRequestConfig) => {

        let data = JSON.parse(req.data);

        let translateApiErrorKey = "";
        let key = "";
        if (data.email.startsWith('serverError')) {
          key = "server";
          translateApiErrorKey = "server.error";
        } else {
          TRANSLATE_API_ERROR.forEach((v:string, k:string) => {
            if (k.endsWith(data.email.split('@')[0])) {
              translateApiErrorKey = k;
              key = data.email.split('.')[1];
            }
          });
        }

        if (translateApiErrorKey !== "") {
            let response: any = {
              "@context": "\/api\/contexts\/Error",
              "@type": "hydra:Error",
              "hydra:title": "An error occurred",
              "hydra:description": key + ': ' +translateApiErrorKey
            };

            if (key !== 'server') {
              response.violations = [{
                propertyPath: key,
                message: translateApiErrorKey,
              }]
            }

            console.log("[MOCK] api/subscriptions => 400", response, req)
            return [400, response]
        }

        console.log("[MOCK] api/subscriptions => 200", req)
        return [200, data]
    });
    console.log(`[MOCK] configure: api/subscriptions:
    if email startWhith errorServer return 400 with api-platform error violation
    if email startWhith ${Array.from(TRANSLATE_API_ERROR.keys())} return 400 with api-platform error
    else return 200`)

    mock.onPost('/api/invitations').reply(async (req: AxiosRequestConfig) => {
      let data = JSON.parse(req.data);

      let translateApiErrorKey: {key:string, error: string}[] = [];
      if (data.emails.some((e: string) => e.startsWith('serverError'))) {
        translateApiErrorKey = [{key:"server", error: "server.error"}];
      } else {
        TRANSLATE_API_ERROR.forEach((v:string, k:string) => {
          data.emails.forEach((e: string) => {
            if (k.endsWith(e.split('@')[0])) {
              translateApiErrorKey.push({key:e.split('.')[1], error: k});
            }
          })
        });
      }

      if (translateApiErrorKey.length > 0) {
        let response: any = {
          "@context": "\/api\/contexts\/Error",
          "@type": "hydra:Error",
          "hydra:title": "An error occurred",
          "hydra:description": translateApiErrorKey.map(t => `${t.key}: ${t.error}`).join(',')
        };

        if (translateApiErrorKey.some((t) => t.key !== 'server' )) {
          response.violations = translateApiErrorKey.map(t =>{
            return {
              propertyPath: t.key,
              message: t.error,
            }
          })
        }

        console.log("[MOCK] api/invitations => 400", response, req)
        return [400, response]
      }

      return [200, data]
    });


  /**
     * on any => ok
     */
    mock.onAny().reply(async (req: AxiosRequestConfig) => {
        console.log("[MOCK] api/*", req)
        return [200]
    })
    console.log("[MOCK] configure: api/* return 200 {}")
}

export default (context: Context, inject: (key: string, value: any) => void) => {
    var api = new Api(context.store.$envStore.config.API_URL)
    var mock = new MockAdapter(api.axios, { onNoMatch: undefined , delayResponse: 2000 });
    if (context.store.$envStore.config["MOCK"]) {
        inject('api', api);
        console.log("[MOCK] plugin api mock loaded")

        configMock(mock)
    }

}
