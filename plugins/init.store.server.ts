import { Context } from '@nuxt/types'
import { config } from "dotenv";


export default async (context : Context) => {
    let conf = config({
      path: process.env.APP_ENV ? `.env.${process.env.APP_ENV}` : ".env"
    });
    try {
      let confMap = conf.parsed || {};
      context.store.$envStore.init(confMap as {[key:string] : string});
    } catch (e) {
      console.log(e)
    }
}
