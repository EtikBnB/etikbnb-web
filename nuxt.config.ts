import { Configuration } from '@nuxt/types'

export default {
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    script: [
      { src: 'https://static.airtable.com/js/embed/embed_snippet_v1.js' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxt/typescript-build',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/style-resources',
    '@nuxtjs/axios',
    '@nuxtjs/proxy'
  ],
  plugins: [
    {
      src: '~/plugins/init.store.server.ts',
      mode: "server"
    },
    {
      src: '~/plugins/api.client.ts',
      mode: "client"
    }
  ].concat(process.env.NODE_ENV === "development" ? [
    {
      src: '~/plugins/api.mock.ts',
      mode: "client"
    }
  ] : []),
  axios: {
    proxy: true
  },
  render: {
    ssrLog: false
  },
  proxy: {
    '/api/': { target: 'http://workspace:8000', pathRewrite: { '^/': '' }, changeOrigin: true }
  },
  server: {
    host: process.env.NUXT_HOST || 'localhost',
    port: process.env.NUXT_PORT || 3000
  },
  /*
  ** Build configuration
  */
  styleResources: {
    scss: [
      './assets/scss/main-utils.scss',
    ]
  },
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
} as Configuration;
