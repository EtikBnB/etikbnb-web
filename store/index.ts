import { Store } from 'vuex'
import { getModule } from 'vuex-module-decorators'
import  { EnvStore }from '~/stores/EnvStore';


let envStore: EnvStore;
let store: Store<any>;

export const plugins = [async (storex: Store<any>) => {
  storex.registerModule('envStore', EnvStore);
  envStore = getModule(EnvStore, storex);

  storex.$envStore = envStore;
  store = storex;
}];

export {
  store
}
