import Vue from 'vue'
import Faq from '~/components/faq/Faq'
import Subscribe from '~/components/subscribe/Subscribe'
import Partners from '~/components/partners/Partners'

export default Vue.extend({
  components: {
    Subscribe,
    Faq,
    Partners
  }
})
