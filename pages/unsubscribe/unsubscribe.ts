import {Vue, Component} from 'vue-property-decorator'

@Component({
})
export default class Unsubscribe extends Vue {
  email: string = '';
  sending: boolean = false;
  unsubscribed: boolean = false;

  errors: string[] = [];

  async unsubscribe() {
    this.sending = true;

    try {
      await this.$api.delete('api/subscriptions/', this.email);
      this.unsubscribed = true
    } catch (e) {
      this.errors.push("submit");
    } finally {
      this.sending = false
    }
  }
}
