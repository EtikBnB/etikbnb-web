import {Vue, Component, Prop} from 'vue-property-decorator'

@Component({
})
export default class Alert extends Vue {

  @Prop({type: String, default: undefined})
  message!: string;
  @Prop({type: String, default: ''})
  id!: string;

  @Prop({type: Boolean, default: false})
  error!: Boolean;
  @Prop({type: Boolean, default: false})
  info!: Boolean;
  @Prop({type: Boolean, default: false})
  success!: Boolean;

  isVisible(): boolean {
    return !!this.$slots.default || (this.message != undefined && this.message !== '');
  }

  getType() {
    if(this.error) {
      return "error"
    } else if(this.success) {
      return "success"
    } else if(this.info) {
      return "info"
    }
    return '';
  }

  getImage() {
    if(this.error) {
      return require("~/assets/images/thinking.png")
    } else if(this.success) {
      return require("~/assets/images/thumbsup.png")
    } else if(this.info) {
      return ""
    }
    return '';
  }
}
