import { Vue, Component } from 'vue-property-decorator'
import Invite from "~/components/invite/Invite";
import Alert from '~/components/alert/Alert'

@Component({
  components: {
    Invite,
    Alert
  }
})
export default class Subscribe extends Vue {
  liame: string = '';
  type: string | null = null;
  types: Array<any> = [
    { label: "Proposer un hebergement", value: "offer", id: "id-ask" },
    { label: "Faire une demande d'hebergement", value: "ask", id: "id-offer" },
    { label: "Soutenir le projet", value: "support", id: "id-support" }
  ];
  errorMessages: Array<any> = [
    { key: "liame", value: "Le champ email est obligatoire et doit être rempli selon le format suivant : email@email.fr."},
    { key: "type", value: "Ce champ est obligatoire, choisissez une option."},
    { key: "accepted", value: "Vous devez accepter les conditions pour vous inscrire."}
  ];
  accepted: boolean = false;
  errors: Array<string> = [];
  subscribed: boolean = false;
  sending: boolean = false;

  email: string = '';
  name: string = '';

  apiSubscriptionErr: string[] = [];

  mounted(){
    console.log(this.$store.$envStore.config.API_URL)
    console.log(this.$store.$envStore.config.MOCK)
  }

  checkForm(e: Event): boolean {
    e.preventDefault();
    if (this.sending) {
      return false;
    }
    this.errors = [];

    if (!this.liame) {
      this.errors.push('liame')
    }
    if (!this.type) {
      this.errors.push('type')
    }
    if (!this.accepted) {
      this.errors.push('accepted')
    }

    if (this.errors.length == 0 && this.email == '' && this.name == '') {
      this.sendByApi()
    }
    return false;
  }

  hasError(name: string) {
    return this.errors.some(e => e === name)
  }

  getError(name: string) {
    if (this.hasError(name)) {
      let errorMessage = this.errorMessages.find(e => e.key === name)
      return errorMessage.value || "undefined";
    }
    return undefined
  }

  async sendByApi() {
    this.sending = true;
    this.apiSubscriptionErr = []

    try {
      var r = await this.$api.post('api/subscriptions', { email: this.liame, purpose: this.type })
      this.type = '';
      this.accepted = false;
      (r) ? this.subscribed = true : this.errors.push("submit")
    } catch(e){
      if (e) {
        this.apiSubscriptionErr = (typeof e === "string") ? [e] : e;
      } else {
        this.apiSubscriptionErr = ["Une erreur s'est produite lors de la souscription."] ;
      }
    } finally {
      this.sending = false
    }
  }
}
