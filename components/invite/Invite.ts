import { Vue, Component, Prop } from 'vue-property-decorator'
import Alert from '~/components/alert/Alert'

@Component({
  components: {Alert}
})
export default class Invite extends Vue {

  @Prop({ type: String, default: '' })
  sender!: string;

  errors: Array<string> = [];
  sending: boolean = false;
  sended: boolean = false;
  errorMessage: string = '';
  firstname: string = '';
  lastname: string = '';
  invitation: Invitation = {
    sender: this.sender,
    firstname: '',
    lastname: '',
    emails: ['']
  } as Invitation;

  errorMessages: any = {
    emptyEmails: "les emails ne doivent pas être vides",
    notUniqueEmails: "Les emails dans la liste doivent être unique.",
    missSender: "Votre mail est manquant."
  };

  apiInvitationErr = [];

  hasError(name: string) {
    return this.errors.some(e => e === name)
  }

  addEmail() {
    if (this.invitation.emails.length < 10) {
      this.invitation.emails.push('')
    }
  }

  deleteEmail(index: number) {
    this.invitation.emails = this.invitation.emails.filter((e,i) => i !== index);
  }

  submit() {
    if (this.sending) {
      return false;
    }
    this.errors = [];
    let emails = this.invitation.emails.filter(e => e !== '');
    if (emails.length == 0) {
      this.errors.push('emptyEmails')
    }

    let uniqueArray = Array.from(new Set(emails));
    if (uniqueArray.length !== emails.length) {
      this.errors.push('notUniqueEmails')
    }

    if (this.sender === '') {
      this.errors.push('missSender')
    }

    if (this.errors.length == 0) {
      this.invitation.emails = emails;
      this.send()
    }
  }

  async send() {
    this.sending = true;
    this.apiInvitationErr = []

    try {
      let r = await this.$api.post('api/invitations', this.invitation) as Invitation
      (r) ? this.sended = true : this.errors.push("submit")
    } catch (e) {
      this.apiInvitationErr = (e) ? ((typeof e === 'string') ? [e] : e) : ["Une erreur s'est produite lors de l'envoie de l'invitation."] ;
    } finally {
      this.sending = false
    }
  }
}
