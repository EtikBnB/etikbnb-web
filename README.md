# etikbnb-web

## Technology

Nuxtjs : framework for vuejs  
docker-compose.yml : dev mode (start server in watch mode)  
Dockerfile : test and production mode (build dist with node and copy it in nginx container)  

## Start dev with docker-compose
The default api is localhost:3000 with proxy to redirect on localhost:8000 which is the local docker-compose stack api  
To do that, uncomment networks blocks in docker-compose.yml  
Run server as dev mode (watched) : 
```
docker-compose up -d
```

## Start with docker

### build docker image

#### build dev
``` 
docker build -t etikbnb-web .
```
`NODE_ENV=development` it is a default value

#### build prod

``` 
docker build --build-arg NODE_ENV=production -t etikbnb-web .
```

### Start
```
docker run -p 80:80 etikbnb-web
```
env vars (with default value):
* `APP_ENV=`         # see file .env.$NODE_ENV
* `NUXT_HOST=0.0.0.0`  # to listen on all interfaces
* `NUXT_PORT=80`

#### Start locally with docker
``` 
docker-compose up -d
```
`APP_ENV=` it is a default value
all API calls are mocked. cf : `plugins/api.mock.ts`
example to force error on call API : 
pre-subscription => 
    errorServer@test.fr => display globally error message 
    preinscription.email.alreadyExist@test.fr => display error email already exists
Details are available in code `plugins/api.mock.ts`

#### Start locally with docker without mocked
``` 
APP_ENV=local docker-compose up -d
```

#### Start for test env with docker
``` 
docker run -p 80:80 -e "APP_ENV=test" etikbnb-web
```

#### Start for production env with docker
``` 
docker run -p 80:80 -e "APP_ENV=production" etikbnb-web
```

## Build Setup Nuxtjs

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
